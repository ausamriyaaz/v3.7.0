---
bookCollapseSection: true
weight: 3
---

# Mobile Applications

A mobile application is a software application specifically created to run on **mobile devices**. Entgra IoT Server enables managing Android, iOS, and Windows mobile applications.

Entgra IoT Server supports the following two UIs to help **Mobile App Creators**/**Publishers** manage mobile applications: 

*   **App Publisher**: This UI enables you to create and manage mobile applications.

*   **App Store**: This UI enables you to install and update mobile applications on mobile devices. It also comes with social features such as rating and liking that help Mobile App Creators to understand the popularity and usability of their mobile applications.

For more information on mobile application management: 

*   For a quick hands-on experience see [Publishing Applications](https://entgra-documentation.gitlab.io/v3.7.0/docs/quick-start-guide/android-device#publishing-applications). 

*   You can also try the tutorials to create [Android](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/mobile-application-management/creating-an-android-application/), [iOS](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/mobile-application-management/creating-an-ios-application/), and [Web Clip](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/mobile-application-management/creating-a-web-clip/) applications, to create [versions](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/mobile-application-management/creating-a-new-version-of-the-mobile-application/), and to [install](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/mobile-application-management/installing-an-application-on-a-device/) them. 

*   For detailed information on managing mobile applications, see [Managing Mobile Applications](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/Managing-Mobile-Applications/)  
