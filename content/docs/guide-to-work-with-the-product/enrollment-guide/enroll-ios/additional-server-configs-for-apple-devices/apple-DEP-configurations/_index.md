---
bookCollapseSection: true
weight: 2
---

# Apple(iOS and MacOS) server configurations

Device Enrollment Program (DEP) is a program provided by Apple to allow device management solutions to have control over corporate-owned devices. Let's take a look at what you need to do, to get started and understand why you need DEP.


{{< hint info >}}
<strong>Pre-requisites</strong>
<ul style="list-style-type:disc;">
    <li><a href="#apple-server-configurations">Apple server configurations must be complete  Entgra</a></li>
</ul>
{{< /hint >}}

## Benefits of using DEP
<ul style="list-style-type:disc;">
    <li>Provides zero touch device enrolments for IT administrators.</li>
    <li>Provides better control over iOS devices and provides the features described in the features section.</li>
    <li>Restrict the user from removing EMM management from the device.</li>
</ul>

## Enrolling in the Apple Device Enrollment Program

The first step is to enroll your organization with Apple Device Enrollment Program (DEP) and link Entgra IoT Server's EMM solution to your DEP portal. Follow the steps given below to enroll with DEP.


{{< expand "Steps" "..." >}}
1. Follow the four steps under  Enroll in Apple Deployment Programs  in the <a href="https://images.apple.com/business/docs/DEP_Guide.pdf">Apple guide</a> to Create an account by providing basic information about your business including a D‑U‑N‑S number. The enrollment process is free of charge.

2. Add an administrator to the Apple DEP portal as instructed under  Getting Started with the Device Enrollment Program- step 1 in the <a href="https://images.apple.com/business/docs/DEP_Guide.pdf">Apple guide</a>.


<ul style="list-style-type:disc;">
The following information is required:

<li>Provides zero touch device enrolments for IT administrators.First and last name of the individual enrolling on behalf of the business.</li>

{{< hint info >}}
NOTE: This must be a legal, human name. First and last names such as “IT Coordinator” or “iPad Deployment” will not be accepted.

{{< /hint >}}

<li>A work email address that isn’t associated with an iTunes or iCloud account, and that hasn’t been used as an Apple ID for any other Apple service or website.</li>

{{< hint warning >}}
CAUTION: Don’t use this new Apple ID with an iTunes or iCloud account, or any other Apple services or website other than the Volume Purchase Program. Doing so causes the Apple ID to stop working with all Apple Deployment Programs.
{{< /hint >}}

<li>Work phone number.</li>

<li>Title/Position</li>
</ul>


{{< /expand >}}

## Generate a DEP token to link EMM with Apple

Now that the DEP account is created, EMM server must be made aware of this DEP account. In order to allow EMM server to talk to Apple DEP servers and link with the DEP account, a special server token has to be generated from Apple DEP portal and passed to EMM server.

{{< hint info >}}
<strong>Prerequisites</strong>
<ul style="list-style-type:disc;">
    <li><a href="https://www.openssl.org/source/"> Download and install OpenSSL. </a> Linux OS's have OpenSSL installed by default.</li>
</ul>
{{< /hint >}}

{{< expand "Steps" "..." >}}
### Step 1
A key pair has to be generated on your machines. Create a new directory and create a file named openssl.cnf in the directory you just created. Copy the content given below to the openssl.cnf and save it.

```bash
[ v3_req ]# Extensions to add to a certificate request
basicConstraints=CA:TRUE
keyUsage = digitalSignature, keyEncipherment
  
[ v3_ca ]
# Extensions for a typical CA
# PKIX recommendation.
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
# This is what PKIX recommends but some broken software chokes on critical
# extensions.
basicConstraints = critical,CA:true
# So we do this instead.
#basicConstraints = CA:true
# Key usage: this is typical for a CA certificate. However since it will
# prevent it being used as an test self-signed certificate it is best
# left out by default.
keyUsage = digitalSignature, keyCertSign, cRLSign
```

### Step 2
Using command prompt, navigate into the directory and run the commands given below in the given order.

```bash
openssl genrsa -out dep_private.key 4096
 
openssl req -new -key dep_private.key -out dep.csr
 
openssl x509 -req -days 365 -in dep.csr -signkey dep_private.key -out dep.crt -extensions v3_ca -extfile ./openssl.cnf
 
openssl x509 -in dep.crt -out dep.pem
```

Now, you see the dep.pem file created in the directory you created.

### Step 3
Navigate to the Apple business manager and sign in with your organization's Apple credentials.

{{< hint info >}}
Do not close this browser session until you are done configuring the DEP portal. If you do close the browser session, you need to enter the verification code again and start configuring the DEP portal from where you stopped.
{{< /hint >}}

<ul style="list-style-type:disc;">
    <li>In the left sidebar click "Settings" and click "MDM Servers".</li>
    <li>Then click "Add MDM Server".</li>
    <li>To the "MDM server name" text box, please enter your MDM name(Example: Entgra IoT server)</li>
    <li>Under MDM server settings section click "choose a file" and upload the dep.pem file created in step 2.</li>
    <li>Click "save" to save the details and from the next screen, click "Download token" then click "Download server token" to download the token used to link.</li>
</ul>
{{< hint info >}}
The DEP server token expires in an year (365 days). Therefore, you need to renew it when it expires. For more information on how to renew the expired token, see Renewing the DEP Server Token.
{{< /hint >}}
{{< /expand >}}

## Linking the Entgra with Apple DEP

You can configure the device startup settings(activation) of the iOS devices, to skip configurations or include additional configurations. This is done by creating profiles in Entgra IoT Server and assigning it to the devices.

{{< expand "Steps" "..." >}}
## Step 1

- Navigate to the folder where you saved the Apple server token you downloaded when Adding the WSO2 EMM Solution to the DEP Portal via the terminal.

- Decrypt the server token using the command given below. You see the token.json file created in the same directory.

```bash
openssl smime -decrypt -in "<THE-.PM7-TOKEN-SERVER-FILE-NAME>.pm7" -inkey "dep_private.key" > token.json
```

- Login to devicemgt portal and go to following section.
    CONFIGURATION MANAGEMENT > PLATFORM CONFIGURATIONS > iOS Configurations.

![image](../../../../../image/2015.png) 

<table class="tableizer-table">
    <thead>
        <tr class="tableizer-firstrow">
            <th>Field</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Agent App ID</td>
            <td>        
            You are able to enroll and iOS device with WSO2 IoT Server's EMM solution, with or without the agent.

            <strong>If you are not using the agent</strong>, you can leave this section blank and skip to the next field.

            <br><br>
            <strong>If you are using the agent</strong>, you would need to publish the agent application through the publisher portal of your tenant
            and provide the app Id of the published application here. follow the steps given below:

            <ul style="list-style-type:disc;">
            Configure WSO2 IoT Server to install iOS mobile applications:
                <li>Open the <IOTS_HOME>/conf/application-mgt.xml file.</li>
                <li>Add https as the value for the MDMConfig > ArtifactDownloadProtocol property.</li>
                <li>Sign in to WSO2 IoT Server's App Publisher console: https://<IOTS_SERVER_HOST>:9443/publisher.</li>
                <li>The default username is admin and the default password is admin, and the default IOTS_SERVER_HOST is localhost.</li>
                <li>Create a new application and upload the iOS agent ipa file.</li>
                <li>Move this application to published state.</li>
                <li>Once the application is created, click on the application.</li>
                <li>Note down the App ID from the URL.
            Example: https://172.20.10.12:9443/publisher/asset/mobileapp/667026af-2ed4-426f-95c3-246a5707db66</li>
                <li>Enter the App ID as the value for Agent App ID.</li>
            </ul>
            </td>
        </tr>
        <tr>
            <td>Consumer Key</td>
            <td>Open the token.json file you just generated and enter the value given for theconsumer_keyhere.</td>
        </tr>
        <tr>
            <td>Consumer Secret</td>
            <td>Enter the value given for theconsumer_secretin thetoken.jsonfile.</td>
        </tr>
        <tr>
            <td>Access Token</td>
            <td>Enter the value given foraccess_tokenin thetoken.jsonfile.</td>
        </tr>
        <tr>
            <td>Access Secret</td>
            <td>Enter the value given foraccess_secret in thetoken.jsonfile, here.</td>
        </tr>
        <tr>
            <td>Access Token Expiry</td>
            <td>Enter the value given foraccess_token_expiryin thetoken.jsonfile, here.</td>
        </tr>
    </tbody>
</table>
{{< /expand >}}

## Adding Devices to the Apple DEP Portal

The Apple Device Enrollment Program (DEP) allows iOS devices purchased through the Apple DEP program and devices purchased outside the DEP program to be enrolled as supervised devices to a mobile device management system. You need to add the devices to the Apple DEP portal to link the devices with DEP system.

### Adding devices purchased via DEP

{{< expand "Steps" "..." >}}
1. Sign in to the Apple DEP account.
2. Under Manage Devices, click Choose Devices By.  There are three options in this section:

Serial Number: Provide the serial number of the devices.

Order Number: Provide the purchase order number of the devices.

Upload CSV File: Upload a CSV file that contains a list of serial numbers of those devices needed to be enrolled. 

{{< /expand >}}

### Adding iOS devices using the Apple configurator

The devices purchased outside DEP can be added manually to the DEP account. You need to plug in each device to a Mac and factory reset the device using THE Apple Configurator. 
Follow the steps given below:

{{< hint info >}}
<strong>Pre-requisites</strong>
<ul style="list-style-type:disc;">
    <li>Devices that have an OS that is iOS 11 or higher.</li>
    <li>Apple Configurator 2.5 or higher.</li>
</ul>
{{< /hint >}}

{{< expand "Steps" "..." >}}
<ul style="list-style-type:disc;">
    <li>Plug in your iOS device to the Mac, and open the Apple Configurator 2.5.</li>
    <li>Click Prepare to prepare the device to be added to DEP. </li>
    <img src="../../../image/2016.png">
    <li>Enter the details:For Prepare with, select Manual Configurations. Select only the Add to Device Enrollment Program option. Click Next.</li>
     <img src="../../../image/2017.png">
    <li>Select New Server and click Next.</li>
    <li>Enter a name, keep the default URL, and click Next.</li>
    <li>You are directed to a screen that give the following error message: Unable to verify the server's enrollment URL. </li>
     <img src="../../../image/2018.png">
    <li>Do not worry about it, click Next.</li>
    <li>Click Next again, enter the DEP account's username and password, and click Sign In.</li>
     <img src="../../../image/2019.png">
</ul>

{{< /expand >}}

## Creating and assigning activation profiles to Devices


{{< expand "Steps" "..." >}}
### Step 1

Login to devicemgt portal

### Step 2

Click CONFIGURATION MANAGEMENT > DEP CONFIGURATIONS > Add Profile.

<img src="../../../image/2020.png">

<table class="tableizer-table">
    <thead>
        <tr class="tableizer-firstrow">
            <th>Field</th>
            <th>Description</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Enter profile name</td>
            <td>Provide a name for your profile.</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Enter profile URL</td>
            <td>Enter the URL to access the WSO2 IoT Server's EMM server.<br>
The URL needs to be in the following format: https://<IOTS_HOST>:8243/api/ios/v1.0/profile-dep/. If you port offset WSO2 IoT Server's core profile, make sure to offset the port defined here too. In a setup where hostnames are used, please replace the <IOTS_HOST>:8243 with the gateway hostname.</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Is supervised</td>
            <td>If selected, the device is set to the Supervised mode. The supervised devices are also referred to as DEP enabled device. The EMM administrators are able to carry out operations on the device as they are owned by the organization.</td>
        </tr>
        <tr>
            <td>Is MDM removable</td>
            <td>If selected, the device user is unable to unregister the device from the WSO2 IoT Server's EMM solution.</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Is mandatory</td>
            <td>If selected, the device users need to complete enrolling their devices with WSO2 IoT Server during the setup, and cannot skip the step.
Important: It is highly adviced to keep this unchecked during testing to avoid getting locked out due to a misconfiguration. When moving to a production environment, this can be checked after testing.</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Department</td>
            <td>Enter the department the device belongs to. This value is displayed when the device is starting up.</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Support phone number</td>
            <td>Enter the support number. This is provided during the setup if device users require help or run into issues.</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Support email address</td>
            <td>Enter the support email address. This is provided during the setup if device users require help or run into issues.</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Anchor certs</td>
            <td><strong>Production environment</strong>: You don't need to enter any value here because WSO2 IoT Server has a valid SSL certificate in a production environment.<br><br>

<strong>Testing/Development environment</strong>: If the testing environment does not have a valid SSL certificate, follow the steps given to get the values you need to enter:

{{< hint info >}}
You need to ensure that you have configured WSO2 IoT Server with the iOS features before running the commands given below as mentioned in the before you begin section.
{{< /hint >}}

Navigate to the <IOTS_HOME>/ios-configurator/output directory via the terminal. This directory is available only if you configured WSO2 IoT Server with the iOS features.
<ul style="list-style-type:disc;">


<li>Run the command given below to convert the SSL certificate you had already generated when configuring iOS features to the .pem format.</li>

openssl x509 -in ia.crt -out ia_cert.pem

<li>As per the Apple specifications, encode the ca_cert.pem file you generated when configuring the iOS features.</li>

openssl x509 -in ca_cert.pem -outform DER|base64
<li>As per the Apple specifications, encode the ia_cert.pem file you generated above.</li>

openssl x509 -in ia_cert.pem -outform DER|base64
<li>Enter the values you got in step c and step d as comma separated values:
Format:
&lt;STEP_C_VALUES&gt;, &lt;STEP_D_VALUE&gt;
</li>
</ul>
</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Skip setup items</td>
            <td>If Skip setup items is selected, the device automatically skips through all the pages that appear at the time of setting up an iOS device.<br>

If only specific items are selected, the device skips the selected pages at the time of setting up the iOS device.
For example, if you select passcode and Siri, you don't have to enter a passcode or set up Siri at the time of setting up the iOS device for the first time.</td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>

### Step 3

Click Add to add the configured profile.

### Step 4
Navigate to the Device list page.

### Step 5
Click Sync if you haven't done before to get the list of devices that are enrolled as DEP devices from the DEP portal.

### Step 6
Assign a profile to a device. The settings in the profile are used when starting up the device for the first time.

{{< /expand >}}

## Enrolling DEP Devices

Please refer <a href="../ios-DEP-enrollment">iOS DEP Device enrollment</a> section.









