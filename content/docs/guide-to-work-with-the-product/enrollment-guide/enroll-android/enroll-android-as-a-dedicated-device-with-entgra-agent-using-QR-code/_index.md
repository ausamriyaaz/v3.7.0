---
bookCollapseSection: true
weight: 4
---

# Enroll Android as a dedicated device with Entgra agent using QR code

{{< hint info >}}
<strong>Pre-requisites</strong><br>
<ul style="list-style-type:disc;">
    <li>Server is <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/download-and-start-the-server-/">downloaded and started</a></li>
    <li>Logged into the server's <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/login-to-devicemgt-portal/">device mgt portal</a></li>
    <li>Optionally, basic <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/key-concepts/#android ">concepts of Android device management</a> will be beneficial as well. </li>
</ul>
{{< /   hint >}}



<iframe width="560" height="315" src="https://www.youtube.com/embed/4OckRsN_IYU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Set platform configurations</li>
    <li>Click "Enroll Device" and select device type</li>
    <li>In the server Select device ownership as "COSU (KIOSK)"</li>
    <li>Scan QR code that is generated in server</li>
    <li>When the policy agreement is shown, click "Agree" to proceed</li>
    <li>User will be prompted to agree to few permissions and click "Allow"</li>
    <li>Agree to using data usage monitoring to allow server to check the data usage of the device</li>
    <li>Allow agent to change do not disturb status which is used to ring the device</li>
    <li>Enter and confirm a PIN code, which will be needed by admin to perform any critical tasks with user concent. Then click "Set PIN Code" to complete enrollment</li>
</ul>
